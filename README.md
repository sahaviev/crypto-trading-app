# Crypto Trading App

### Cover Letter

Despite the fact that I have a lot of elements implemented before
5 days very small amount of time to implement technical task with all best practices.

I hope this project will be enough to demonstrate my skills.

---

### Instructions

Install dependencies `yarn`

Run application `yarn start`

Run all tests `yarn test`

Run unit-tests `yarn test:unit`

Run e2e-tests `yarn test:e2e`

---

**Used Libraries**:
1. axios - for xhr requests
2. react-query - hook for queries fetching
3. react-router-dom - for routing
4. zustand - state management
5. rooks - useful hooks
6. clsx - css class merging

**Linting & Testing**:
1. @mvp-tech/lint-presets - configurations for linters and stylelint
2. eslint
3. prettier
4. stylelint
5. jest - for unit-testing
6. cypress - e2e-testing

**Styling**
1. scss
2. css-modules

---

Waiting for your feedback!

Regards,
**Rail Sakhaviev**.
