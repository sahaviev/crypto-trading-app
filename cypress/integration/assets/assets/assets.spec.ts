import assetsServiceFind200 from "./mocks/assets-service-find-200.json";

describe("Assets page", () => {
  beforeEach(() => {
    cy.intercept(
      {
        hostname: "data.messari.io",
        path: "/api/v2/assets*",
        method: "GET",
      },
      (req) => {
        req.reply({
          statusCode: 200,
          delay: 0,
          body: JSON.stringify(assetsServiceFind200),
        });
      }
    ).as("findAssets");

    cy.visit("/");
    cy.wait(["@findAssets"]);
  });

  context("Assets Table", () => {
    it("should render assets table correctly", () => {
      cy.findByText("Assets • 10").should("exist");
      cy.findByTestId("assets-table")
        .find("tbody")
        .within(() => {
          cy.get("tr").should("have.length", 10);
          cy.get("tr")
            .eq(0)
            .within(() => {
              cy.get("td").should("have.length", 4);
              cy.get("td").eq(0).should("have.text", "Bitcoin");
              cy.get("td").eq(1).should("have.text", "BTC");
              cy.get("td").eq(2).should("have.text", "38345.512564850134");
              cy.get("td").eq(3).contains("a", "Buy/Sell");
            });
        });
    });

    it("should has load more assets button and load assets on click", () => {
      cy.findByText("Load More Assets").should("exist").click();
      cy.findByText("Assets • 20").should("exist");
      cy.findByTestId("assets-table")
        .find("tbody")
        .find("tr")
        .should("have.length", 20);
    });

    // ToDo: add testing for sorting
  });
});
