import assetsServiceFind200 from "../assets/mocks/assets-service-find-200.json";

import tradeServiceGetExchangeRates200 from "./mocks/trade-service-get-exchange-rates-200.json";

describe("Trade page", () => {
  beforeEach(() => {
    cy.intercept(
      {
        hostname: "data.messari.io",
        path: "/api/v2/assets*",
        method: "GET",
      },
      (req) => {
        req.reply({
          statusCode: 200,
          delay: 0,
          body: JSON.stringify(assetsServiceFind200),
        });
      }
    ).as("findAssets");

    cy.intercept(
      {
        hostname: "rest.coinapi.io",
        path: "v1/exchangerate/*",
        method: "GET",
      },
      (req) => {
        req.reply({
          statusCode: 200,
          delay: 0,
          body: JSON.stringify(tradeServiceGetExchangeRates200),
        });
      }
    ).as("getExchangeRates");

    cy.visit("/trade");
  });

  context("Trade Form", () => {
    it("should render trade form login required message", () => {
      cy.findByText("You have to sign in for trading.");
    });

    it("should render trade form after login", () => {
      cy.findByText("Sign In").click({ force: true });
      cy.findByPlaceholderText("Username").type("demo");
      cy.findByPlaceholderText("Password").type("demo");
      cy.contains("button", "Login").click();
    });
  });
});
