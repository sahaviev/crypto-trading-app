import create from "zustand";

import { Auth } from "../types/auth";

export const useAuthStore = create<Auth>((set) => ({
  authenticated: localStorage.getItem("authenticated") === "true" || false,
  username: localStorage.getItem("username"),
  setAuth: (authenticated, username) => {
    localStorage.setItem("authenticated", authenticated.toString());
    localStorage.setItem("username", username);
    set(() => ({
      authenticated,
      username,
    }));
  },
}));
