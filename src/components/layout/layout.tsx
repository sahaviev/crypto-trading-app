import { FC } from "react";

import styles from "./layout.module.scss";

export const Layout: FC = ({ children }) => {
  return <section className={styles.layout}>{children}</section>;
};
