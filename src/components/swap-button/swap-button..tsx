import { FC } from "react";

import { ReactComponent as Change } from "../../assets/change.svg";

import styles from "./swap-button.module.scss";

type Props = {
  onClick: () => void;
};

export const SwapButton: FC<Props> = ({ onClick }) => {
  return (
    <button className={styles.swap} type="button" onClick={onClick}>
      <Change className={styles.icon} />
    </button>
  );
};
