import { ChangeEvent, useState } from "react";
import { Link } from "react-router-dom";

import { ASSETS_PER_PAGE } from "../../consts";
import { routes } from "../../routes";
import { Button } from "../button/button";

import { useAssets } from "./queries";
import { getSortedAssets, sortings, SortType } from "./sortings";

import styles from "./assets.module.scss";

export const Assets = () => {
  const assetPages = useAssets();
  const [filter, setFilter] = useState<SortType>(SortType.Default);

  const handleFilterChange = (evt: ChangeEvent<HTMLSelectElement>) => {
    setFilter(Number(evt.currentTarget.value));
  };

  return (
    <div className={styles.assets}>
      <div className={styles.header}>
        <div className={styles.title}>
          {`Assets • ${
            assetPages.isSuccess &&
            assetPages.data.pages.length * ASSETS_PER_PAGE
          }`}
        </div>
        <div className={styles.title}>
          Sortings
          <select
            className={styles.sortingSelect}
            value={filter}
            onChange={handleFilterChange}
          >
            <option value={SortType.Default}>
              {sortings[SortType.Default]}
            </option>
            <option value={SortType.Name}>{sortings[SortType.Name]}</option>
            <option value={SortType.Price}>{sortings[SortType.Price]}</option>
          </select>
        </div>
      </div>
      <div className={styles.scrollWrapper}>
        <table className={styles.table} data-testid="assets-table">
          <thead className={styles.thead}>
            <tr className={styles.tableHeaderRow}>
              <th>Coin Name</th>
              <th>Symbol</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {assetPages.isSuccess &&
              getSortedAssets(assetPages.data.pages.flat(), filter).map(
                (asset) => (
                  <tr key={asset.id} className={styles.tableHeaderRow}>
                    <td>
                      <img
                        src={`https://messari.io/asset-images/${asset.id}/32.png?v=2`}
                        alt={`${asset.name} icon`}
                        className={styles.assetImage}
                      />
                      {asset.name}
                    </td>
                    <td>{asset.symbol}</td>
                    <td>{asset.metrics.market_data.price_usd}</td>
                    <td>
                      <Link to={routes.TRADE}>Buy/Sell</Link>
                    </td>
                  </tr>
                )
              )}
          </tbody>
        </table>
      </div>
      {assetPages.isLoading && <div className={styles.loading}>Loading...</div>}
      <div className={styles.controls}>
        <Button onClick={assetPages.fetchNextPage}>Load More Assets</Button>
      </div>
    </div>
  );
};
