import { useInfiniteQuery } from "react-query";

import { AssetsService } from "../../services/assets-service";
import { Asset } from "../../types/asset";

export const useAssets = () => {
  return useInfiniteQuery<Asset[]>(
    ["assets"],
    async (params) => {
      return AssetsService.find({
        page: params.pageParam ? params.pageParam.page : 1,
        fields: "symbol,name,id,slug,metrics/market_data/price_usd",
      });
    },
    {
      getNextPageParam: (_, pages) => ({
        page: pages.length + 1,
      }),
      keepPreviousData: true,
    }
  );
};
