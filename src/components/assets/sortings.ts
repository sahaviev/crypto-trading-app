import { Asset } from "../../types/asset";

export enum SortType {
  Default,
  Name,
  Price,
}

export const sortings = {
  [SortType.Default]: "Default",
  [SortType.Name]: "Name",
  [SortType.Price]: "Price",
};

export const getSortedAssets = (assets: Asset[], filter: SortType) => {
  switch (filter) {
    case SortType.Name:
      return [...assets].sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });
    case SortType.Price:
      return [...assets].sort(
        (a, b) =>
          b.metrics.market_data.price_usd - a.metrics.market_data.price_usd
      );
    default:
      return assets;
  }
};
