import { getSortedAssets, SortType } from "./sortings";

const assets = [
  {
    id: "21c795f5-1bfd-40c3-858e-e9d7e820c6d0",
    metrics: { market_data: { price_usd: 2744.131034444057 } },
    market_data: { price_usd: 2744.131034444057 },
    price_usd: 2744.131034444057,
    name: "Ethereum",
    slug: "ethereum",
    symbol: "ETH",
  },
  {
    id: "362f0140-ecdd-4205-b8a0-36f0fd5d8167",
    metrics: { market_data: { price_usd: 1.0523870776107405 } },
    market_data: { price_usd: 1.0523870776107405 },
    price_usd: 1.0523870776107405,
    name: "Cardano",
    slug: "cardano",
    symbol: "ADA",
  },
  {
    id: "1e31218a-e44e-4285-820c-8282ee222035",
    metrics: { market_data: { price_usd: 38385.461680107226 } },
    market_data: { price_usd: 38385.461680107226 },
    price_usd: 38385.461680107226,
    name: "Bitcoin",
    slug: "bitcoin",
    symbol: "BTC",
  },
];

describe("getSortedAssets", () => {
  it("should return same assets on sort by default", () => {
    const sortedAssets = getSortedAssets(assets, SortType.Default);
    expect(sortedAssets).toEqual(assets);
  });

  it("should sort assets by name correctly", () => {
    const expected = [
      {
        id: "1e31218a-e44e-4285-820c-8282ee222035",
        metrics: { market_data: { price_usd: 38385.461680107226 } },
        market_data: { price_usd: 38385.461680107226 },
        price_usd: 38385.461680107226,
        name: "Bitcoin",
        slug: "bitcoin",
        symbol: "BTC",
      },
      {
        id: "362f0140-ecdd-4205-b8a0-36f0fd5d8167",
        metrics: { market_data: { price_usd: 1.0523870776107405 } },
        market_data: { price_usd: 1.0523870776107405 },
        price_usd: 1.0523870776107405,
        name: "Cardano",
        slug: "cardano",
        symbol: "ADA",
      },
      {
        id: "21c795f5-1bfd-40c3-858e-e9d7e820c6d0",
        metrics: { market_data: { price_usd: 2744.131034444057 } },
        market_data: { price_usd: 2744.131034444057 },
        price_usd: 2744.131034444057,
        name: "Ethereum",
        slug: "ethereum",
        symbol: "ETH",
      },
    ];
    const sortedAssets = getSortedAssets(assets, SortType.Name);
    expect(sortedAssets).toEqual(expected);
  });

  it("should sort assets by price correctly", () => {
    const expected = [
      {
        id: "1e31218a-e44e-4285-820c-8282ee222035",
        metrics: { market_data: { price_usd: 38385.461680107226 } },
        market_data: { price_usd: 38385.461680107226 },
        price_usd: 38385.461680107226,
        name: "Bitcoin",
        slug: "bitcoin",
        symbol: "BTC",
      },
      {
        id: "21c795f5-1bfd-40c3-858e-e9d7e820c6d0",
        metrics: { market_data: { price_usd: 2744.131034444057 } },
        market_data: { price_usd: 2744.131034444057 },
        price_usd: 2744.131034444057,
        name: "Ethereum",
        slug: "ethereum",
        symbol: "ETH",
      },
      {
        id: "362f0140-ecdd-4205-b8a0-36f0fd5d8167",
        metrics: { market_data: { price_usd: 1.0523870776107405 } },
        market_data: { price_usd: 1.0523870776107405 },
        price_usd: 1.0523870776107405,
        name: "Cardano",
        slug: "cardano",
        symbol: "ADA",
      },
    ];
    const sortedAssets = getSortedAssets(assets, SortType.Price);
    expect(sortedAssets).toEqual(expected);
  });
});
