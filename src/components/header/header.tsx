import { useState } from "react";

import { ReactComponent as LoginIcon } from "../../assets/login.svg";
import { ReactComponent as UserIcon } from "../../assets/user.svg";
import { useAuthStore } from "../../store/auth";
import { LoginModal } from "../login-modal/login-modal";

import styles from "./header.module.scss";

export const Header = () => {
  const { authenticated, username } = useAuthStore();
  const [loginModalVisible, setLoginModalVisible] = useState(false);

  const handleLoginButtonClick = () => {
    setLoginModalVisible(true);
  };

  const closeLoginModal = () => {
    setLoginModalVisible(false);
  };

  return (
    <header className={styles.header}>
      <div className={styles.welcomeMessage}>
        {authenticated && "Welcome Back"}
        {!authenticated && (
          <>
            For sign in use: <span className={styles.username}>demo/demo</span>
          </>
        )}
      </div>
      <div className={styles.loginWrapper}>
        {!authenticated && (
          <button
            className={styles.loginButton}
            onClick={handleLoginButtonClick}
          >
            <LoginIcon />
            Sign In
          </button>
        )}
        {authenticated && (
          <div className={styles.authenticatedWrapper}>
            <div className={styles.iconWrapper}>
              <UserIcon />
            </div>
            <span className={styles.username}>{username}</span>
          </div>
        )}
      </div>
      <LoginModal show={loginModalVisible} onClose={closeLoginModal} />
    </header>
  );
};
