import React, { ChangeEvent, FC, FormEvent } from "react";

import { Asset } from "../../types/asset";

import styles from "./currency-input.module.scss";

type Props = {
  value: string;
  placeholder: string;
  onValueChange: (value: string) => void;
  onCurrencyChange?: (currency: Asset) => void;
  selectedCurrency?: Asset;
  currencies?: Record<string, Asset>;
};

const CurrencyInputComponent: FC<Props> = ({
  value,
  placeholder,
  onValueChange,
  onCurrencyChange,
  selectedCurrency,
  currencies,
}) => {
  const handleValueChange = (evt: FormEvent<HTMLInputElement>) => {
    if (evt.currentTarget.validity.valid) {
      onValueChange(evt.currentTarget.value);
    }
  };

  const handleCurrencyChange = (evt: ChangeEvent<HTMLSelectElement>) => {
    if (onCurrencyChange && currencies) {
      onCurrencyChange(currencies[evt.currentTarget.value]);
    }
  };

  return (
    <div className={styles.wrapper}>
      <label className={styles.label}>
        Amount ({selectedCurrency && selectedCurrency.symbol})
      </label>
      <input
        className={styles.input}
        type="text"
        pattern="[0-9.]*"
        placeholder={placeholder}
        value={value}
        onChange={handleValueChange}
      />
      {currencies && (
        <select
          className={styles.currencySelector}
          onChange={handleCurrencyChange}
          value={selectedCurrency && selectedCurrency.id}
        >
          {Object.values(currencies).map((currency) => (
            <option key={currency.id} value={currency.id}>
              {currency.name}
            </option>
          ))}
        </select>
      )}
    </div>
  );
};

export const CurrencyInput = React.memo(CurrencyInputComponent);
