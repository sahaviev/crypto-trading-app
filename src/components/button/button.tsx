import { FC } from "react";
import clsx from "clsx";

import styles from "./button.module.scss";

type Props = {
  className?: string;
  onClick: () => void;
};

export const Button: FC<Props> = ({ className, onClick, children }) => {
  return (
    <button
      type="button"
      className={clsx(styles.button, className)}
      onClick={onClick}
    >
      {children}
    </button>
  );
};
