import { useQuery } from "react-query";

import { AssetsService } from "../../services/assets-service";
import { Asset } from "../../types/asset";

export const useAssets = () => {
  return useQuery<Asset[]>(
    ["trade-form-assets"],
    async () => {
      return AssetsService.find({
        page: 1,
        limit: 15,
        fields: "symbol,name,id,metrics/market_data/price_usd",
      });
    },
    {
      keepPreviousData: true,
    }
  );
};
