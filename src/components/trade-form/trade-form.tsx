import { useCallback, useEffect, useMemo, useState } from "react";
import { useThrottle } from "rooks";

import { ExchangeRateService } from "../../services/exchange-rate-service";
import { Asset } from "../../types/asset";
import { CoinapiRates } from "../../types/coinapi-rates";
import { convertArrayToObject } from "../../utils/convert-array-to-object";
import { Button } from "../button/button";
import { CurrencyInput } from "../currency-input/currency-input";
import { SwapButton } from "../swap-button/swap-button.";

import { useAssets } from "./queries";

import styles from "./trade-form.module.scss";

const THROTTLING_TIMEOUT = 1000;

export const TradeForm = () => {
  const assetsQuery = useAssets();
  const assetsData = assetsQuery.data;
  const currencies = useMemo(() => {
    return assetsData ? convertArrayToObject<Asset>(assetsData, "id") : {};
  }, [assetsData]);
  const [rates, setRates] = useState<CoinapiRates | undefined>(undefined);
  const [firstCurrency, setFirstCurrency] = useState<Asset | undefined>();
  const [secondCurrency, setSecondCurrency] = useState<Asset | undefined>();
  const [firstInput, setFirstInput] = useState("");
  const [secondInput, setSecondInput] = useState("");

  const [getExchangeRatesThrottled] = useThrottle(
    async (from: string, to: string) => {
      const newRates = await ExchangeRateService.getExchangeRates({
        from,
        to,
      });
      setRates(newRates);
    },
    THROTTLING_TIMEOUT
  );

  const handleTradeClick = () => {
    console.log("how much bitcoin will cost in the end of 2022? =)");
  };

  const handleFirstInputChange = useCallback(
    (value: string) => {
      setFirstInput(value);
      if (rates) {
        setSecondInput((Number(value) * rates.rate).toString());
      }
    },
    [rates]
  );

  const handleSecondInputChange = useCallback(
    (value: string) => {
      setSecondInput(value);
      if (rates) {
        setFirstInput((Number(value) / rates.rate).toString());
      }
    },
    [rates]
  );

  const handleFirstCurrencyChange = useCallback((currency: Asset) => {
    setRates(undefined);
    setFirstCurrency(currency);
  }, []);

  const handleSecondCurrencyChange = useCallback((currency: Asset) => {
    setRates(undefined);
    setSecondCurrency(currency);
  }, []);

  const handleSwapClick = () => {
    if (firstInput !== "") {
      handleSecondInputChange(firstInput);
    }
  };

  useEffect(() => {
    if (assetsData) {
      setFirstCurrency(assetsData[0]);
      setSecondCurrency(assetsData[4]);
    }
  }, [assetsData]);

  useEffect(() => {
    if (firstCurrency && secondCurrency) {
      getExchangeRatesThrottled(firstCurrency.symbol, secondCurrency.symbol);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [firstCurrency, secondCurrency]);

  return (
    <div className={styles.tradeForm}>
      <CurrencyInput
        placeholder="0.0"
        value={firstInput}
        onValueChange={handleFirstInputChange}
        selectedCurrency={firstCurrency}
        onCurrencyChange={handleFirstCurrencyChange}
        currencies={currencies}
      />
      <SwapButton onClick={handleSwapClick} />
      <CurrencyInput
        placeholder="0.0"
        value={secondInput}
        onValueChange={handleSecondInputChange}
        selectedCurrency={secondCurrency}
        onCurrencyChange={handleSecondCurrencyChange}
        currencies={currencies}
      />
      {rates && firstCurrency && secondCurrency && (
        <div className={styles.rate}>
          {`1 ${firstCurrency.symbol} = ${rates.rate.toFixed(9)} ${
            secondCurrency.symbol
          }`}
        </div>
      )}
      <Button onClick={handleTradeClick} className={styles.button}>
        Buy BTC
      </Button>
    </div>
  );
};
