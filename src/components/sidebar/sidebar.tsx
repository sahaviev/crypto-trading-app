import { NavLink } from "react-router-dom";
import clsx from "clsx";

import { routes } from "../../routes";

import styles from "./sidebar.module.scss";

const navigationLinks = [
  {
    title: "Home",
    link: routes.HOME,
  },
  {
    title: "Trade",
    link: routes.TRADE,
  },
];

export const Sidebar = () => {
  return (
    <section className={styles.sidebar}>
      <header className={styles.header}>CRYPTO TRADING</header>
      <nav>
        <div className={styles.title}>Main Menu</div>
        <ul className={styles.menu}>
          {navigationLinks.map((item) => (
            <li key={item.link}>
              <NavLink
                to={item.link}
                className={({ isActive }) =>
                  clsx(styles.item, isActive && styles.active)
                }
              >
                {item.title}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
    </section>
  );
};
