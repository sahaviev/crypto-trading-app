import { FC, useEffect, useRef } from "react";
import ReactDOM from "react-dom";

export const Portal: FC = ({ children }) => {
  const portalRoot = document.getElementById("body"); // ToDo: hardcode
  const el = useRef(document.createElement("div"));
  const current = el.current;

  if (!portalRoot) {
    throw Error("PortalRoot should be element");
  }

  useEffect(() => {
    portalRoot.appendChild(current);
    return () => {
      portalRoot.removeChild(current);
    };
  }, [current, portalRoot]);

  return ReactDOM.createPortal(children, el.current);
};
