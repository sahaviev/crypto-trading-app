import { FC, FormEvent, useEffect, useState } from "react";

import { ReactComponent as CloseIcon } from "../../assets/close.svg";
import { useAuthStore } from "../../store/auth";
import { Button } from "../button/button";
import { Input } from "../input/input";
import { Modal } from "../modal/modal";

import styles from "./login-modal.module.scss";

type Props = {
  show: boolean;
  onClose: () => void;
};

export const LoginModal: FC<Props> = ({ show, onClose }) => {
  const { authenticated, setAuth } = useAuthStore();
  const [form, setFormValue] = useState({
    username: "",
    password: "",
  });

  const handleLoginClick = () => {
    if (form.username === "demo" && form.password === "demo") {
      setAuth(true, "demo");
    }
  };

  const handleChange = (evt: FormEvent<HTMLInputElement>) => {
    setFormValue({
      ...form,
      [evt.currentTarget.name]: evt.currentTarget.value,
    });
  };

  useEffect(() => {
    if (authenticated) {
      onClose();
    }
  }, [authenticated, onClose]);

  return (
    <Modal show={show} onClose={onClose} className={styles.modal}>
      <button
        type="button"
        className={styles.close}
        onClick={onClose}
        aria-label="Close"
      >
        <CloseIcon className={styles.closeIcon} />
      </button>
      <div className={styles.body}>
        <Input
          placeholder="Username"
          className={styles.usernameInput}
          name="username"
          value={form.username}
          onChange={handleChange}
        />
        <Input
          placeholder="Password"
          type="password"
          className={styles.passwordInput}
          name="password"
          value={form.password}
          onChange={handleChange}
        />
        <div className={styles.controls}>
          <Button onClick={handleLoginClick}>Login</Button>
        </div>
      </div>
    </Modal>
  );
};
