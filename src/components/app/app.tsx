import { BrowserRouter, Route, Routes } from "react-router-dom";

import { Home } from "../../pages/home/home";
import { Trade } from "../../pages/trade/trade";
import { routes } from "../../routes";

import styles from "./app.module.scss";

const App = () => {
  return (
    <div className={styles.wrapper}>
      <BrowserRouter>
        <Routes>
          <Route path={routes.HOME} element={<Home />} />
          <Route path={routes.TRADE} element={<Trade />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
