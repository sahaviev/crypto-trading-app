import { FC, useRef } from "react";
import clsx from "clsx";
import { useOutsideClick } from "rooks";

import { Portal } from "../portal/portal";

import styles from "./modal.module.scss";

type Props = {
  className?: string;
  show: boolean;
  onClose: () => void;
  overlay?: boolean;
};

export const Modal: FC<Props> = ({
  className,
  onClose,
  show,
  overlay = true,
  children,
}) => {
  const ref = useRef<HTMLDivElement | null>(null);

  useOutsideClick(ref, onClose);

  if (!show) {
    return null;
  }

  return (
    <Portal>
      <div className={styles.wrapper}>
        <div ref={ref} className={clsx(className, styles.container)}>
          {children}
        </div>
      </div>
      {overlay && <div className={styles.backdrop} />}
    </Portal>
  );
};
