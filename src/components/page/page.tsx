import { FC } from "react";

import { Header } from "../header/header";
import { Layout } from "../layout/layout";
import { Sidebar } from "../sidebar/sidebar";

import styles from "./page.module.scss";

export const Page: FC = ({ children }) => {
  return (
    <Layout>
      <Sidebar />
      <div className={styles.wrapper}>
        <Header />
        <aside className={styles.content}>{children}</aside>
      </div>
    </Layout>
  );
};
