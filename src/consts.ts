const { REACT_APP_COINAPI_KEY } = process.env;

export const ASSETS_PER_PAGE = 10;

export const COINAPI_KEY = REACT_APP_COINAPI_KEY || ""; // it's commited in git only for demo reason!
