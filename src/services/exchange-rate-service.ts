import axios from "axios";

import { COINAPI_KEY } from "../consts";
import { CoinapiRates } from "../types/coinapi-rates";

/**
 * CoinAPI cryptocurrencies exchange REST API
 * @link: https://docs.coinapi.io/#md-rest-api
 */
const axiosInstance = axios.create({
  baseURL: "https://rest.coinapi.io/v1/",
  headers: {
    "Content-type": "application/json",
    "X-CoinAPI-Key": COINAPI_KEY,
  },
});

type ParamsData = {
  from: string;
  to: string;
};

const getExchangeRates = async ({
  from,
  to,
}: ParamsData): Promise<CoinapiRates> => {
  return axiosInstance
    .get<CoinapiRates>(`/exchangerate/${from}/${to}`)
    .then((response) => response.data);
};

const ExchangeRateService = {
  getExchangeRates,
};

export { ExchangeRateService };
