import axios from "axios";

import { ASSETS_PER_PAGE } from "../consts";
import { Asset } from "../types/asset";

/**
 * Messari's Crypto Data API (1.0.0)
 * @link: https://messari.io/api/docs
 */
const axiosInstance = axios.create({
  baseURL: "https://data.messari.io/api/v2/",
  headers: {
    "Content-type": "application/json",
  },
});

type ResponseData = {
  data: Asset[];
};

type ParamsData = {
  page?: number;
  limit?: number;
  fields?: string;
};

const find = async ({
  page = 1,
  limit = ASSETS_PER_PAGE,
  fields = "",
}: ParamsData): Promise<Asset[]> => {
  return axiosInstance
    .get<ResponseData>("/assets", {
      params: {
        page,
        limit,
        fields,
      },
    })
    .then((response) => response.data.data);
};

const AssetsService = {
  find,
};

export { AssetsService };
