export const convertArrayToObject = <T extends Record<string, unknown>>(
  array: T[],
  key: keyof T
) => {
  return array.reduce<Record<string, T>>((obj, item) => {
    obj[item[key] as string] = item;
    return obj;
  }, {});
};
