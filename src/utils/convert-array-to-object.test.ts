import { convertArrayToObject } from "./convert-array-to-object";

type Example = {
  id: string;
  name: string;
  age: number;
};

describe("convertArrayToObject", () => {
  it('should convert array of objects to object with key in "id" correctly', () => {
    const examples = [
      {
        id: "1",
        name: "Name 1",
        age: 31,
      },
      {
        id: "2",
        name: "Name 2",
        age: 18,
      },
    ];
    const expected = {
      "1": {
        id: "1",
        name: "Name 1",
        age: 31,
      },
      "2": {
        id: "2",
        name: "Name 2",
        age: 18,
      },
    };
    const object = convertArrayToObject<Example>(examples, "id");
    expect(object).toMatchObject(expected);
  });
});
