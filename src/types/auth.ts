export type Auth = {
  authenticated: boolean;
  username?: string | null;
  setAuth: (authenticated: boolean, name: string) => void;
};
