export type Asset = {
  symbol: string;
  name: string;
  id: string;
  slug: string;
  metrics: {
    market_data: {
      price_usd: number;
    };
  };
};
