import { Page } from "../../components/page/page";
import { TradeForm } from "../../components/trade-form/trade-form";
import { useAuthStore } from "../../store/auth";

export const Trade = () => {
  const { authenticated } = useAuthStore();

  return (
    <Page>
      {!authenticated && "You have to sign in for trading."}
      {authenticated && <TradeForm />}
    </Page>
  );
};
