import { Assets } from "../../components/assets/assets";
import { Page } from "../../components/page/page";

export const Home = () => {
  return (
    <Page>
      <Assets />
    </Page>
  );
};
